# OBJETIVOS

Realizar una aplicación completa con Laravel por fases

# SISTEMA DE INFORMACION

Aplicación que me permita controlar las practicas a entregar en varios cursos .
Queria tener los alumnos y las practicas que tienen que entregar y almacenar la nota del alumno

De los cursos quiero almacenar la siguiente información

- Id
- Nombre curso
- Fecha comienzo
- Numero de horas
- Observaciones

De las practicas necesito:

- Id
- Titulo
- Fichero
- Curso

De los alumnos necesito:

- Id
- Nombre
- Apellidos
- Fecha nacimiento
- Email (único)
- Foto
- Curso

# DISEÑO DE LA BASE DE DATOS

Para empezar desarrollamos el diagrama semantico (E/R) de la aplicacion

![alt text](image-2.png)

# CONVERSION DEL MODELO RELACIONAL

![alt text](image-3.png)

# INSTRALAR APLICACION BASE DE LARAVEL

~~~php
laravel new aplicacion7
~~~

# INSTALAR COMPONENTES

Instalar sass y bootstrap

Instalar el componente de idioma español para los mensajes de error
~~~php

~~~

# CONFIGURACION DE LA APLICACION

En el archivo .env colocamos idioma y nombre de la aplicacion

# CREAMOS CONTROLADORES, MODELOS Y MIGRACIONES 

Creamos los controladores, modelos, migraciones, seeder y factory de todas las tablas

~~~php
php artisan make:model Nombre -mfscr
~~~

# TERMINAMOS LOS MODELOS

-   Creamos una propiedad protected con el nombre de la tabla

~~~php
//nombre de la tabla
    protected $table = 'alumnos';
~~~

-   Creamos una propiedad protected para indicar los campos de asignacion masiva

~~~php
//campos de asignacion masiva
    protected $fillable = [
        'nombre',
        'apellidos',
        'fechanacimiento',
        'email',
        'foto',
    ];
~~~
-   Creamos una propiedad estatica labels para colocar las etiquetas a los campos 

~~~php
//creamos atributo estatico con los labels
    public static $labels = [
        'id' => 'ID del Alumno',
        'nombre' => 'Nombre',
        'apellidos' => 'Apellidos',
        'fechanacimiento' => 'Fecha de nacimiento',
        'email' => 'Email',
        'foto' => 'Foto',
    ];
~~~

-   Campos personalizados:
    -   metodo getAttributeLabel
    ~~~php
    //metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }
    ~~~
    -   metodo getFields
    ~~~php
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }
    ~~~

# EN LOS MODELOS AÑADIMOS LAS RELACIONES

~~~php
    //voy a crear las relaciones entre tablas
    //como la relacion es de muchos se pone en plural
    public function perteneces(): HasMany
    {
        //pertenece a muchos y devuelve la ruta de la relacion
        return $this->hasMany(Pertenece::class);
    }

    //relacion uno a muchos
    public function presentas(): HasMany
    {
        return $this->hasMany(Presenta::class);
    }
~~~

# TERMINAMOS LOS ARCHIVOS DE MIGRACION

Para que no nos de errores primero haremos las que no rtienen claves ajenas para poder hacer las relaciones

Estructura de todas las tablas y relaciones de la base de datos
~~~php
 /**
     * Archivo App\database\migrations\create_alumnos_table
     */
    public function up(): void
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->string('apellidos', 200);
            $table->date('fechanacimiento');
            $table->string('email', 100);
            $table->string('foto');
            $table->timestamps();
            $table->unique(['email'], 'email_unique'); //el email debe ser unico y el nombre
        });
    }
~~~

Tenemos que ir colocando las claves ajenas
~~~php
    /**
     * Archivo App\database\migrations\create_presentas_table
     */
    public function up(): void
    {
        Schema::create('presentas', function (Blueprint $table) {
            $table->id();
            $table->float('nota')->nullable();
            //claves ajenas alumno
            $table->unsignedBigInteger('alumno_id');
            $table
                ->foreign('alumno_id')
                ->references('id')
                ->on('alumnos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            //claves ajenas practica
            $table->unsignedBigInteger('practica_id');
            $table
                ->foreign('practica_id')
                ->references('id')
                ->on('practicas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }
~~~

# EJECUTAMOS LAS MIGRACIONES

~~~php
php artisan migrations
~~~

# PARA CREAR LOS DATOSD DE PRUEBA CON FAKER

Primero las tablas sin relaciones:

-   Creamos los campos en los factory

~~~php
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre' => fake()->name(),
            'apellido' => fake()->lastName(),
            'email' => fake()->unique()->safeEmail(),
            'fechanaciomiento' => fake()->date(),
            'fotos' => fake()->image(),
        ];
    }
~~~

-   Indicamos en los seeder cuantos crear

~~~php
/**
     * Run the database seeds.
     */
    public function run(): void
    {
        Alumno::factory(30)->create();
    }
~~~

Podemos usar los factories sin necesidad de los seeders

~~~php
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
 //llamar a los factories desde aqui sin necesidad de seeders
        Alumno::factory(10)->create();
        Curso::factory(10)->create();
    }
}
~~~






