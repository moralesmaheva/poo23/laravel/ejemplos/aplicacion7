@extends('layouts.main')

@section('content')
    <h1>Pertenece</h1>
<div class="listado">
    @foreach ($perteneces as $pertenece)
        <div class="tarjeta">
            <ul>
                <li>{{ $pertenece->id }}</li>
                <li>Alumno: {{ $pertenece->alumno_id }} {{ $pertenece->alumno->nombre }} {{ $pertenece->alumno->apellidos }}
                </li>
                <li>perteneces: {{ $pertenece->curso_id }} {{ $pertenece->curso->nombre }}</li>
            </ul>
            <div class="botones">
                <a href="{{ route('pertenece.show', $pertenece) }}" class="boton">Ver</a>
                <a href="{{ route('pertenece.edit', $pertenece) }}" class="boton">Editar</a>
                <form action="{{ route('pertenece.destroy', $pertenece) }}" method="post" id="eliminar">
                    @csrf
                    @method('delete')
                    <button type="submit" class="boton">Borrar</button>
                </form>
            </div>
        </div>
    @endforeach
</div>
@endsection
