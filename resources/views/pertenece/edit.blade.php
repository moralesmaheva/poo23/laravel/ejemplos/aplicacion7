@extends('layouts.main')

@section('content')
<div>
    <h1>Cambiar alumno o curso</h1>
</div>
    <form action="{{ route('pertenece.update', $pertenece) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div>
            <label>Selecciona el Alumno</label>
            <select name="alumno_id">
                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}" 
                        {{ old('alumno_id', $pertenece->alumno_id) == $alumno->id ? 'selected' : '' }}>
                        {{ $alumno->id }} - {{ $alumno->nombre }} {{ $alumno->apellidos }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label>Selecciona el curso</label>
            <select name="curso_id">
                @foreach ($cursos as $curso)
                <option value="{{ $curso->id }}" 
                    {{ old('curso_id', $pertenece->curso_id) == $curso->id ? 'selected' : '' }}>
                    {{ $curso->id }} - {{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <button type="submit" class="boton">Actualizar</button>
        </div>
    </form>
@endsection