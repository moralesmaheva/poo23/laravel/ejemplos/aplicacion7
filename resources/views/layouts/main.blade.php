<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite('resources/css/app.scss')
    @yield('css')
</head>

<body>
    <div class="menu">
        <ul>
            <li><a href="{{ route('inicio') }}"
                class="{{ request()->routeIs('inicio') ? 'active' : '' }}">Inicio</a></li>
            <li><a href="{{ route('alumno.index') }}"
                class="{{ request()->routeIs('alumno.index') ? 'active' : '' }}">Gestion Alumnos</a></li>
            <li><a href="{{ route('alumno.create') }}"
                class="{{ request()->routeIs('alumno.create') ? 'active' : '' }}">Crear alumno</a></li>
            <li><a href="{{ route('curso.index') }}"
                class="{{ request()->routeIs('curso.index') ? 'active' : '' }}">Gestion Cursos</a></li>
            <li><a href="{{ route('curso.create') }}"
                class="{{ request()->routeIs('curso.create') ? 'active' : '' }}">Crear curso</a></li>
            <li><a href="{{ route('practica.index') }}"
                class="{{ request()->routeIs('practica.index') ? 'active' : '' }}">Gestion practicas</a></li>
            <li><a href="{{ route('practica.create') }}"
                class="{{ request()->routeIs('practica.create') ? 'active' : '' }}">Crear practica</a></li>
            <li><a href="{{ route('pertenece.index') }}"
                class="{{ request()->routeIs('pertenece.index') ? 'active' : '' }}">Pertenencia alumnos a cursos</a></li>
            <li><a href="{{ route('pertenece.create') }}"
                class="{{ request()->routeIs('pertenece.create') ? 'active' : '' }}">Asignar curso a alumno</a></li>
            <li><a href="{{ route('presenta.index') }}"
                class="{{ request()->routeIs('presenta.index') ? 'active' : '' }}">Gestion de entregas</a></li>
            <li><a href="{{ route('presenta.create') }}"
                class="{{ request()->routeIs('presenta.create') ? 'active' : '' }}">Agregar entrega</a></li>
        </ul>
    </div>
    <div>
        @yield('content')
    </div>
    <div class="footer">
        Aplicacion creada por Maheva Morales
    </div>
</body>
@vite('resources/js/app.js')
@yield('js')
</html>
