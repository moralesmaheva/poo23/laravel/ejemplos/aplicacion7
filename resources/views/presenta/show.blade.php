@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <ul>
            <li>{{ $presentum->id }}</li>
            <li>Nota: {{ $presentum->nota }}</li>
            <li>Alumno: {{ $presentum->alumno_id }} {{ $presentum->alumno->nombre }} {{ $presentum->alumno->apellidos }}</li>
            <li>Practica: {{ $presentum->practica_id }} {{ $presentum->practica->titulo }}</li>
        </ul>
        <div class="botones">
            <a href="{{ route('presenta.edit', $presentum) }}" class="boton">Editar</a>
            <form action="{{ route('presenta.destroy', $presentum) }}" method="post" id="eliminar">
                @csrf
                @method('DELETE')
                <button type="submit" class="boton">Borrar</button>
            </form>
        </div>
    </div>
@endsection
