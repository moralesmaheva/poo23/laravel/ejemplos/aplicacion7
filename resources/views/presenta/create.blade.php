@extends('layouts.main')

@section('content')
<div>
    <h1>Presenta nueva practica</h1>
</div>
    <form action="{{ route('presenta.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div>
            <label for="nota">Nota</label>
            <input type="number" name="nota" id="nota" value="{{ old('nota') }}">
        </div>
        <div>
            <label>Alumno: </label>
            <select name="alumno_id">
                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}"> {{ $alumno->id }} - {{ $alumno->nombre }} {{ $alumno->apellidos }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label>Practica: </label>
            <select name="practica_id">
                @foreach ($practicas as $practica)
                    <option value="{{ $practica->id }}">{{ $practica->id }} - {{ $practica->titulo }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <button type="submit" class="boton">Guardar</button>
        </div>
    </form>
@endsection
