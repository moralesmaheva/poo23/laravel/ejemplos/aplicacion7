@extends('layouts.main')

@section('content')
<div class="tarjeta">
    <ul>
        <li>Nombre: {{ $curso->nombre }}</li>
        <li>Fecha de comienzo: {{ $curso->fechacomienzo }}</li>
        <li>Duracion: {{ $curso->duracion }}</li>
        <li>Observaciones: {{ $curso->observaciones }}</li>
    </ul>
    <div class="botones">
        <a href="{{ route('curso.edit', $curso->id) }}" class="boton">Editar</a>
        <form action="{{ route('curso.destroy', $curso) }}" method="post" id="eliminar">
            @csrf
            @method('DELETE')
            <button type="submit" class="boton">Borrar</button>
        </form>
    </div>
</div>
@endSection
