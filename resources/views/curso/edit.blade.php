@extends('layouts.main')

@section('content')
    <div>
        <h1>Actualizar curso</h1>
    </div>
    <form action="{{ route('curso.update',$curso) }}" method="post">
        @csrf
        @method('PUT')
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" value="{{ old('nombre', $curso->nombre) }}">
        </div>
        <div>
            <label for="fechacomienzo">Fecha de comienzo</label>
            <input type="date" name="fechacomienzo" id="fechacomienzo"
                value="{{ old('fechacomienzo', $curso->fechacomienzo) }}">
        </div>
        <div>
            <label for="duracion">Duracion</label>
            <input type="number" name="duracion" id="duracion" value="{{ old('duracion', $curso->duracion) }}">
        </div>
        <div>
            <label for="observaciones">Observaciones</label>
            <input type="text" name="observaciones" id="observaciones"
                value="{{ old('observaciones', $curso->observaciones) }}">
        </div>
        <div>
            <button type="submit" class="boton">Guardar</button>
        </div>
    </form>
@endsection
