@extends('layouts.main')

@section('content')
<div>
    <h1>Actualizar practica</h1>
</div>
    <form action="{{ route('practica.update', $practica) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div>
            <label for="titulo">Titulo</label>
            <input type="text" name="titulo" id="titulo" value="{{ old('titulo', $practica->titulo) }}">
        </div>
        <div>
            <label for="fichero">Fichero</label>
            <div>{{ $practica->fichero }}</div>
            <input type="file" name="fichero" id="fichero" value="{{ old('fichero', $practica->fichero) }}">
        </div>
        <div>
            <label>Selecciona el curso</label>
            <select name="curso_id">
                @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}" 
                        {{ old('curso_id', $practica->curso_id) == $curso->id ? 'selected' : '' }}>
                        {{ $curso->id }} - {{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <button type="submit" class="boton">Actualizar</button>
        </div>
    </form>
@endsection