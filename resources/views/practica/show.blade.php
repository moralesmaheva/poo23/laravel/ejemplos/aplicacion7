@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <ul>
            <li>{{ $practica->id }}</li>
            <li>Titulo: {{ $practica->titulo }}</li>
            <li>Fichero: <a href="{{ asset('storage/' . $practica->fichero) }}">
                    {{ $practica->fichero }}</a></li>
            <li>Curso: {{ $practica->curso_id }} {{ $practica->curso->nombre }}</li>
        </ul>
        <div class="botones">
            <a href="{{ route('practica.edit', $practica) }}" class="boton">Editar</a>
            <form>
                <a href="{{ route('practica.confirmar', $practica) }}" class="boton">Eliminar</a>
            </form>
        </div>
    </div>
@endsection
