@extends('layouts.main')

@section('content')
    <h1>Practicas</h1>
<div class="listado">
    @foreach ($practicas as $practica)
        <div class="tarjeta">
            <ul>
                <li>{{ $practica->id }}</li>
                <li>Titulo: {{ $practica->titulo }}</li>
                <li>Fichero: {{ $practica->fichero }}</li>
                <li>Curso: {{ $practica->curso_id }} {{ $practica->curso->nombre }}</li>
            </ul>
            <div class="botones">
                <a href="{{ route('practica.show', $practica) }}" class="boton">Ver</a>
                <a href="{{ route('practica.edit', $practica) }}" class="boton">Editar</a>
                <form action="{{ route('practica.destroy', $practica) }}" method="post" id="eliminar">
                    @csrf
                    @method('delete')
                    <button type="submit" class="boton">Borrar</button>
                </form>
            </div>
        </div>
    @endforeach
</div>
@endsection
