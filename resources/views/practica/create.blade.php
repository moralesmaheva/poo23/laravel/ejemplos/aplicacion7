@extends('layouts.main')

@section('content')
<div>
    <h1>Crear nueva practica</h1>
</div>
    <form action="{{ route('practica.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div>
            <label for="titulo">Titulo</label>
            <input type="text" name="titulo" id="titulo" value="{{ old('titulo') }}">
        </div>
        <div>
            <label for="fichero">Fichero</label>
            <input type="file" name="fichero" id="fichero" value="{{ old('fichero') }}">
        </div>
        <div>
            <label>Selecciona el curso</label>
            <select name="curso_id">
                @foreach ($cursos as $curso)
                    <option value="{{ $curso->id }}">{{ $curso->id }} - {{ $curso->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <button type="submit" class="boton">Guardar</button>
        </div>
    </form>
@endsection
