@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <ul>
            <li>Nombre: {{ $alumno->nombre }}</li>
            <li>Apellidos: {{ $alumno->apellidos }}</li>
            <li>Fecha nacimiento: {{ $alumno->fechanacimiento }}</li>
            <li>Email: {{ $alumno->email }}</li>
            <li>Foto: <img class="imagen" src="{{ asset('storage/' . $alumno->foto) }}"></li>
        </ul>
        <div class="botones">
            <a href="{{ route('alumno.edit', $alumno->id) }}" class="boton">Editar</a>
            <form action="{{ route('alumno.destroy', $alumno) }}" method="post" id="eliminar">
                @csrf
                @method('DELETE')
                <button type="submit" class="boton">Borrar</button>
            </form>
            </li>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .imagen{
            max-width: 100%;
            width: 100px;
        }
    </style>
@endsection
        