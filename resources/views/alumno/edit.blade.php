@extends('layouts.main')

@section('content')
<div>
    <h1>Actualizar alumno</h1>
</div>
<div class="tarjeta">
    <form action="{{ route('alumno.update',$alumno) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" value="{{ old('nombre', $alumno->nombre) }}">
        </div>
        <div>
            <label for="apellidos">Apellidos</label>
            <input type="text" name="apellidos" id="apellidos" value="{{ old('apellidos', $alumno->apellidos) }}">
        </div>
        <div>
            <label for="fechanacimiento">Fecha de nacimiento</label>
            <input type="date" name="fechanacimiento" id="fechanacimiento" value="{{ old('fechanacimiento', $alumno->fechanacimiento) }}">
        </div>
        <div>
            <label for="email">Email</label>
            <input type="email" name="email" id="email" value="{{ old('email', $alumno->email) }}">
        </div>
        <div>
            <label for="foto">Foto</label>
            <img src="{{ asset('storage/' . $alumno->foto) }}" id="preview">
            <input type="file" name="foto" id="foto" value="{{ old('foto', $alumno->foto) }}" id="fichero">
        </div>
        <div>
        <button type="submit" class="boton">Actualizar</button>
        </div>
    </form>
</div>
@endsection