@extends('layouts.main')

@section('content')
<div>
    <h1>Crear nuevo alumno</h1>
</div>
<div class="tarjeta">
    <form action="{{ route('alumno.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div>
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre" value="{{ old('nombre') }}">
        </div>
        <div>
            <label for="apellidos">Apellidos</label>
            <input type="text" name="apellidos" id="apellidos" value="{{ old('apellidos') }}">
        </div>
        <div>
            <label for="fechanacimiento">Fecha de nacimiento</label>
            <input type="date" name="fechanacimiento" id="fechanacimiento" value="{{ old('fechanacimiento') }}">
        </div>
        <div>
            <label for="email">Email</label>
            <input type="email" name="email" id="email" value="{{ old('email') }}">
        </div>
        <div>
            <label>Foto</label>
            <img id="preview">
            <input type="file" name="foto" id="fichero">
        </div>
        <div>
            <button type="submit" class="boton">Guardar</button>
        </div>
    </form>
</div>
@endsection
