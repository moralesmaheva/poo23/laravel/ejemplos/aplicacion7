<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Practica;
use App\Models\Presenta;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        // User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        //llamar a los factories desde aqui sin necesidad de seeders
        // Alumno::factory(10)->create();
        // Curso::factory(10)->create();

        //creamos 10 cursos y 20 practicas (2 en cada uno)
        // for ($i = 0; $i < 10; $i++) {
        //     $curso = Curso::factory()->create();
        //     Practica::factory()
        //         ->for($curso)
        //         ->create();
        //     Practica::factory()
        //         ->for($curso)
        //         ->create();
        // }

        //llamamos a los seeders con la funcion call
        $this->call([
            AlumnoSeeder::class,
            CursoSeeder::class,
            PracticaSeeder::class,
            PerteneceSeeder::class,
            PresentaSeeder::class
        ]);
    }
}
