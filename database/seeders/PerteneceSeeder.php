<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Pertenece;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PerteneceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //necesitamos un bucle for para crear varios pertenece
        for ($registro = 0; $registro < 10; $registro++) {
            //necesitamos que exista un id de curso 
            //creo un curso
            $curso = Curso::factory()->create();
            //necesitamos que exista un id de alumno
            //creo un alumno
            $alumno = Alumno::factory()->create();
            //necesitamos que exista un id de pertenece
            //creo un nuevo registro con el id de curso y el id de alumno creados
            Pertenece::factory()
                ->for($curso)
                ->for($alumno)
                ->create();
        }
    }
}
