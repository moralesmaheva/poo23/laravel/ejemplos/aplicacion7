<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Pertenece;
use App\Models\Practica;
use App\Models\Presenta;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PresentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //creamos un for para crear varios registros
        for ($registros = 0; $registros < 10; $registros++) {
            //creo un curso
            $curso=Curso::factory()->create();
            //creamos un alumno
            $alumno = Alumno::factory()->create();
            //creamos una practica dentro del curso
            $practica = Practica::factory()
                ->for($curso)
                ->create();
            //creamos un nuevo registro con el alumno y la practica
            Presenta::factory()
                ->for($alumno)
                ->for($practica)
                ->create();

                //indicamos que el alumno esta en el curso
                Pertenece::factory()
                    ->for($alumno)
                    ->for($curso)
                    ->create();
        }
    }
}
