<?php

namespace Database\Seeders;

use App\Models\Curso;
use App\Models\Practica;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PracticaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //creamos 10 cursos y para cada uno creamos un numero aleatorio de practicas
        for ($numeroCurso = 0; $numeroCurso < 10; $numeroCurso++) {
            $curso = Curso::factory()->create();

            //numero de practicas por curso aleatorio
            $totalPracticas = rand(1, 10); //numero entero entre 1 y 10
            for ($numeroPracticas = 0; $numeroPracticas < $totalPracticas; $numeroPracticas++) {
                Practica::factory()
                    ->for($curso)
                    ->create();
            }
        }
    }
}
