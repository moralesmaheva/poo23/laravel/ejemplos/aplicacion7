<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Practica extends Model
{
    use HasFactory;

    //nombre de la tabla
    protected $table = 'practicas';

    //campos de asignacion masiva
    protected $fillable = [
        'titulo',
        'fichero',
        'curso_id',
    ];

    //creamos atributo estatico con los labels
    public static $labels = [
        'id' => 'ID de la practica',
        'titulo' => 'Titulo',
        'fichero' => 'Fichero de la practica',
        'curso_id' => 'Id del curso',
    ];

    //metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    //metodo para devolver todos los campos
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }

    //voy a crear las relaciones entre tablas
    //como la relacion es a uno se pone en singular con belongsTo
    public function curso(): BelongsTo
    {
        return $this->belongsTo(Curso::class);
    }

    //relacion uno a muchos
    public function presentas(): HasMany
    {
        return $this->hasMany(Presenta::class);
    }
}
