<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Alumno extends Model
{
    use HasFactory;

    //nombre de la tabla
    protected $table = 'alumnos';

    //campos de asignacion masiva
    protected $fillable = [
        'nombre',
        'apellidos',
        'fechanacimiento',
        'email',
        'foto',
    ];

    //creamos atributo estatico con los labels
    public static $labels = [
        'id' => 'ID del Alumno',
        'nombre' => 'Nombre',
        'apellidos' => 'Apellidos',
        'fechanacimiento' => 'Fecha de nacimiento',
        'email' => 'Email',
        'foto' => 'Foto',
    ];

    //metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    //metodo para devolver todos los campos
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }

    //voy a crear las relaciones entre tablas
    //como la relacion es de muchos se pone en plural
    public function perteneces(): HasMany
    {
        //pertenece a muchos y devuelve la ruta de la relacion
        return $this->hasMany(Pertenece::class);
    }

    //relacion uno a muchos
    public function presentas(): HasMany
    {
        return $this->hasMany(Presenta::class);
    }
}
