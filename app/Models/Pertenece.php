<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;

class Pertenece extends Model
{
    use HasFactory;

    //nombre de la tabla
    protected $table = 'perteneces';

    //campos de asignacion masiva
    protected $fillable = [
        'alumno_id',
        'curso_id',
    ];

    //creamos atributo estatico con los labels
    public static $labels = [
        'id' => 'ID',
        'alumno_id' => 'ID del Alumno',
        'curso_id' => 'ID del Curso',
    ];

    //metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    //metodo para devolver todos los campos
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }

    //voy a crear las relaciones entre tablas
    //como la relacion es de muchos se pone en plural
    public function alumno(): BelongsTo
    {
        return $this->belongsTo(Alumno::class, 'alumno_id');
    }

    public function curso(): BelongsTo
    {
        return $this->belongsTo(Curso::class, 'curso_id');
    }
}
