<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use App\Models\Practica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PracticaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //listamos las practicas
        $practicas = Practica::all();
        return view('practica.index', compact('practicas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // esto es para mostrar un dropdown en curso_id
        $cursos = Curso::all();

        return view(
            'practica.create',
            compact('cursos')
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //quiero crear un archivo de texto y colocar hola clase
        //quiero almacenarlo en storage

        //este archivo lo crea en el disk default
        //este disco por defecto es local==>storage/app

       // $fichero=Storage::put('hola.txt', 'hola clase');

        //quiero un archivo que se cree en el disco public
        //el dico public apunta a la carpeta storage/app/public

        //$fichero=Storage::disk('public')->put('hola.txt', 'hola clase');
        
        //quiero un archivo que se cree en el disco llamado subidas
        //este disco esta en el config/filesystems.php
        //apunta a la carpeta /public

        //$fichero=Storage::disk('subidas')->put('hola.txt', 'hola clase');
        
        
        $ficheroEnviado=$request->file('fichero');
        //almaceno el fichero
        //el nombre del fichero se genera automaticamente
        //store(nombreCarpeta,disco) lo que hace es una copia del fichero enviado 
        //en el disco indicado
        $fichero=$ficheroEnviado->store('ficheros','public');

        //almaceno el nombre original
        //$nombreOriginal=$ficheroEnviado->getClientOriginalName();

        //almaceno el fichero con el nombre original
        //$fichero=$ficheroEnviado->storeAs('ficheros', $nombreOriginal ,'public');


        //creamos la practica
        //esto crea el registro en la base de datos automaticamente
        //$practica = Practica::create($request->all());

        //opcion 1
        //creamos la practica con request
        // $practica = new Practica($request->all());
        // $practica->fichero=$fichero;
        // $practica->save();

        //opcion 2
        //creamos la practica con fill
        $practica=new Practica();
        $practica->fill($request->all());
        $practica->fichero=$fichero;
        $practica->save();

        //redirigimos
        return redirect()
            ->route('practica.show', $practica);
    }

    /**
     * Display the specified resource.
     */
    public function show(Practica $practica)
    {
        return view('practica.show', compact('practica'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Practica $practica)
    {
        // esto es para mostrar un dropdown en curso_id
        $cursos = Curso::all();

        return view(
            'practica.edit',
            compact('practica', 'cursos')
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Practica $practica)
    {
        //comprobamos si ha subido un fichero
        if($request->hasFile('fichero')){

            //archivo antiguo
            //archivo que esta ya y quiero sustituir
            //$practica->fichero;// String

            //recupero el archivo
            //variable que apunta al fichero subido
            $ficheroEnviado=$request->file('fichero');

            //elimino el archivo antiguo
            Storage::disk('public')->delete($practica->fichero);

            //almaceno el nuevo fichero
            $fichero=$ficheroEnviado->store('ficheros','public');

            //actualizamos la practica
            $practica->fill($request->all());
            //sustituyo el nombre del fichero antiguo por el archivo subido
            $practica->fichero=$fichero;
        }else{
            //si no subo archivo actualizo la practica
            $practica->fill($request->all());
        }

        //graba los cambios
        $practica->save();

        //redirigimos
        return redirect()
            ->route('practica.show', $practica);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Practica $practica)
    {
        //eliminamos el fichero
        Storage::disk('public')->delete($practica->fichero);

        //eliminamos la practica
        $practica->delete();

        //redirigimos
        return redirect()
            ->route('practica.index');
    }

    public function confirmar(Practica $practica)
    {
        return view('practica.confirmar', compact('practica'));
    }
}
