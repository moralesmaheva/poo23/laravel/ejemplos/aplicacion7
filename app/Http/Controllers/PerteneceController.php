<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Pertenece;
use Illuminate\Http\Request;

class PerteneceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $perteneces = Pertenece::all();
        return view('pertenece.index', compact('perteneces'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //tenemos que crear los cursos y alumnos para crear los desplegables
        $cursos = Curso::all();
        $alumnos = Alumno::all();

        //devolvemos la vista
        return view('pertenece.create', compact('cursos', 'alumnos'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //creamos el registro
        $pertenece = Pertenece::create($request->all());
        //redirigimos
        return redirect()
            ->route('pertenece.show', $pertenece);
    }

    /**
     * Display the specified resource.
     */
    public function show(Pertenece $pertenece)
    {
        //devolvemos la vista
        return view('pertenece.show', compact('pertenece'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pertenece $pertenece)
    {
        //tenemos que crear los cursos y alumnos para crear los desplegables
        $cursos = Curso::all();
        $alumnos = Alumno::all();

        //devolvemos la vista
        return view('pertenece.edit', 
        compact('pertenece', 'cursos', 'alumnos'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pertenece $pertenece)
    {
        //actualizamos el registro
        $pertenece->update($request->all());
        //redirigimos
        return redirect()
            ->route('pertenece.show', $pertenece);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pertenece $pertenece)
    {
        //borramos el registro
        $pertenece->delete();
        //redirigimos
        return redirect()
            ->route('pertenece.index');
    }
}
