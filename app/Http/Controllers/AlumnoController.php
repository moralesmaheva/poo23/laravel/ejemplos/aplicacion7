<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //listo los alumnos
        $alumnos = Alumno::all(); //coleccion (objeto de tipo collection) de objetos de tipo alumno
        //vista
        return view('alumno.index', compact('alumnos'));
    }

    /**
     * Solo carga el formulario
     */
    public function create()
    {
        return view('alumno.create');
    }

    /**
     * Esta accion es para crear el alumno
     */
    public function store(Request $request)
    {
        //subir la foto
        $fotoSubida = $request->file('foto');

        //almacenamos la foto
        $foto = $fotoSubida->store('fotos', 'public');

        //crear el alumno automaticamente en la base de datos
        //$alumno=Alumno::create($request->all());

        //crear el alumno con fill en la base de datos
        $alumno = new Alumno();
        $alumno = $alumno->fill($request->all());
        $alumno->foto = $foto;
        $alumno->save();

        //redirigir
        return redirect()
            ->route('alumno.show', $alumno->id)
            ->with('success', 'Alumno creado con exito');
    }

    /**
     * Display the specified resource.
     */
    public function show(Alumno $alumno)
    {
        return view('alumno.show', compact('alumno'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Alumno $alumno)
    {
        return view('alumno.edit', compact('alumno'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Alumno $alumno)
    {
        //comprobamos si ha subido un fichero
        if ($request->hasFile('foto')) {

            //recuperamos la foto antigua
            $fotoSubida = $alumno->foto;
            //eliminamos la antigua
            Storage::disk('public')->delete($alumno->foto);
            //almaceno la nueva foto
            $fotoSubida = $request->file('foto')->store('fotos', 'public');
            //actualizo el alumno
            $alumno->fill($request->all());
            $alumno->foto = $fotoSubida;
        } else {
            //actualizar el alumno
            $alumno->fill($request->all());
        }

        //guardo la modificacion
        $alumno->save();

        //redirigir
        return redirect()
            ->route('alumno.show', $alumno)
            ->with('success', 'Alumno actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Alumno $alumno)
    {
        //eliminamos la foto
        Storage::disk('public')->delete($alumno->foto);
        //eliminamos el alumno
        $alumno->delete();
        //redirigir
        return redirect()
            ->route('alumno.index')
            ->with('success', 'Alumno eliminado con exito');
    }
}
